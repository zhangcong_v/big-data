package com.hxci.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hxci.mapper.UserMapper;
import com.hxci.pojo.User;
import com.hxci.service.UserService;
import com.hxci.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
//spring mvc --->servlet   通过一个地址url找到一个后台方法
@Controller
@RequestMapping("user")
public class UserController {

    @Autowired
    UserService service;

    @ResponseBody
    @RequestMapping(value ="query", produces="text/html; charset=utf-8")
    public String query(){
        List<User> list =service.query();

        return JSONArray.toJSONString(list);
    }
    @ResponseBody
    @RequestMapping("add")
    public String add(User user){
        if(user.getId() == null || ("").equals(user.getId())){
            service.add(user);
        }else{
            service.update(user);
        }

        return "1";
    }

    @ResponseBody
    @RequestMapping("login")
    public String login(User user){
        User res = service.login(user);
        if(res == null) return "0";
        else return "1";

    }





    @ResponseBody
    @RequestMapping("del")
    public String delete(Integer id){
        service.delete(id);
        return "1";
    }
    @ResponseBody
    @RequestMapping(value ="edit", produces="text/html; charset=utf-8")
    public String edit(Integer id){
        User user = service.edit(id);
        return JSONObject.toJSONString(user);
    }


}
